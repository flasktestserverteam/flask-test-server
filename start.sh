#!/usr/bin/env bash
file_path=`pwd`
echo ${file_path}
sudo su - ${1} -c "cd $file_path; /usr/local/bin/gunicorn -w ${2} -b 127.0.0.1:5000 api:application --pid app.pid&"

