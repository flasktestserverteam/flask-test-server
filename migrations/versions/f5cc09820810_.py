"""empty message

Revision ID: f5cc09820810
Revises: 9aac188737ff
Create Date: 2016-09-05 22:31:56.271417

"""

# revision identifiers, used by Alembic.
revision = 'f5cc09820810'
down_revision = '9aac188737ff'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('monitor_logs',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('created_at', sa.TIMESTAMP(), nullable=True),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('monitor_logs')


