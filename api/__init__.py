import os
import sys


from api.libs.utils.provider import Alchemy, App
from threading import Thread
from api.libs.utils.tasks import Task

App.get_app()
App.load_conf()
db = Alchemy.get_db(App.get_app())

from api.app.controllers import *

monitor = Thread(target=Task.db_clean, kwargs=dict(frequency_per_hour=App.get_app().config['MONITOR_FR']), daemon=True)

if os.path.split(sys.argv[0])[0] != 'manager.py':
    monitor.start()

application = App.get_app()
