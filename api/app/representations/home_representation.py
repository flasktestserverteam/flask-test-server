from flask import url_for
from representations.bin.link import Link
from representations.bin.property import Property
from representations.bin.representation import Representation

from api.app.models.hook import Hook
from api.app.models.box import Box


class HomeRepresentation(Representation):

    def __init__(self, models):
        super().__init__(None)
        self.append([
            Link.create(
                href=url_for('index', _external=False),
                rel='self'
            ),
            Link.create(
                href=url_for('boxes', _external=False),
                rel='boxes'
            ),
            Property.create(
                active_boxes=len(Box.query.all()),
                total_hooks=len(Hook.query.all())
            )
        ])