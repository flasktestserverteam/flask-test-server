from representations.bin.action import Action, Field
from representations.bin.link import Link
from representations.bin.property import Property
from representations.bin.representation import Representation
from flask import url_for


class BoxRepresentation(Representation):

    def __init__(self, model):
        super().__init__(model)

        if isinstance(model, (list, tuple)):

            create_box_action = Action.create(
                href=url_for('boxes', _external=False),
                name='create-box',
                method='POST',
                type='application/json'
            )
            create_box_action.append(
                [
                    Field.create(
                        name='expiration-time',
                        value=3600,
                    )
                ]
            )

            self.append([
                    Link.create(
                        href=url_for('boxes', _external=False),
                        rel='self'
                    ),
                    Link.create(
                        href=url_for('index', _external=False),
                        rel='root'
                    ),
                    Property.create(
                        total=len(model),
                    ),
                    create_box_action
                ]
            )

        elif model:
            self.append([
                    Link.create(
                        href=url_for('box', _external=False, box_id=model.id),
                        rel='self'
                    ),
                    Link.create(
                        href=url_for('hooks', box_id=model.id, _external=False),
                        rel='hooks'
                    ),
                    Link.create(
                        href=url_for('index', _external=False),
                        rel='root'
                    ),
                    Link.create(
                        href=url_for('boxes', _external=False),
                        rel='boxes'
                    ),
                    Property.create(
                        total=len(model.hooks.all()),
                        created_at=model.created_at,
                        expiration_date=model.expiration_time
                    )
                ]
            )
