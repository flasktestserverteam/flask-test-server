from datetime import datetime, timedelta

from api.app.representations.box_representation import BoxRepresentation
from api.app.representations.home_representation import HomeRepresentation
from flask import request

from api.app.models.box import Box
from api.libs.utils.provider import App, Alchemy
from flask import make_response

from api.libs.utils.tools import set_content

app = App.get_app()


@app.route('/', methods=['GET'])
def index():
    # system will not store too much data, so can do in-memory
    res = make_response(HomeRepresentation.create(None).dump(), 200)
    res.headers['Content-Type'] = 'application/vnd.siren+json'
    return res


@app.route('/boxes', methods=['GET', 'POST'])
@set_content
# @Alchemy.auto_commit
def boxes():

    if request.method == 'GET':
        res = make_response(BoxRepresentation.create(Box.query.all()).dump(), 200)
        return res
    exp_time = abs(request.json.get('expiration-time', 3600))
    box = Box(expiration_time=(datetime.now() + timedelta(seconds=exp_time)))
    Alchemy.db.session.add(box)
    Alchemy.db.session.commit()
    obj = BoxRepresentation.create(box)
    res = make_response(obj.dump(), 201)
    res.headers['Location'] = [l for l in obj.links if 'self' in l.rel][0].href
    return res


@app.route('/boxes/<string:box_id>', methods=['GET'])
@set_content
def box(box_id):
    res = make_response(BoxRepresentation.create(Box.query.filter_by(id=box_id).first_or_404()).dump(), 200)
    return res