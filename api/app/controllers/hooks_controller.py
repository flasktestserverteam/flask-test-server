import json

from flask import request

from api.app.models.box import Box
from api.app.models.hook import Hook
from api.app.representations.hook_representation import HookRepresentation
from api.libs.utils.provider import App, Alchemy
from flask import make_response

from api.libs.utils.tools import set_content

app = App.get_app()


@app.route('/boxes/<string:box_id>/hooks', methods=['GET', 'POST'])
@set_content
# @Alchemy.auto_commit
def hooks(box_id):
    if request.method == 'GET':
        res = make_response(HookRepresentation.create(Hook.query.filter_by(box_id=box_id).all(), box_id).dump())
        return res
    bx = Box.query.filter_by(id=box_id).first_or_404()
    hk = Hook(
        content=json.dumps(request.json),
        headers=json.dumps(dict(request.headers)),
        url_params=(json.dumps(request.args or dict()))
    )
    bx.hooks.append(hk)
    Alchemy.db.session.commit()
    obj = HookRepresentation.create(hk, box_id)
    res = make_response(obj.dump(), 201)
    res.headers['Location'] = [l for l in obj.links if 'self' in l.rel][0].href
    return res


@app.route('/boxes/<string:box_id>/hooks/<int:hook_id>', methods=['GET'])
@set_content
def hook(box_id, hook_id):
    hook_r = HookRepresentation.create(Hook.query.filter_by(id=hook_id).first_or_404(), box_id)
    res = make_response(hook_r.dump(), 200)
    return res
