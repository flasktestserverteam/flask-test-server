import uuid
from datetime import datetime

from api.libs.utils.provider import Alchemy


class Box(Alchemy.db.Model):

    __tablename__ = 'boxes'

    id = Alchemy.db.Column(Alchemy.db.String, primary_key=True, default=uuid.uuid4)
    expiration_time = Alchemy.db.Column(Alchemy.db.DateTime, unique=False)
    created_at = Alchemy.db.Column(Alchemy.db.TIMESTAMP, default=datetime.now, nullable=False)
    updated_at = Alchemy.db.Column(
        Alchemy.db.TIMESTAMP, default=datetime.utcnow, onupdate=datetime.now, nullable=False
    )
    hooks = Alchemy.db.relationship('Hook', backref='box', cascade='all,delete', lazy='dynamic')

    def __init__(self, expiration_time):
        self.expiration_time = expiration_time
        self.id = str(uuid.uuid4())

    def __repr__(self):
        return '<Box id={1}, expiration_time={0} ...>'.format(self.expiration_time, self.id)

